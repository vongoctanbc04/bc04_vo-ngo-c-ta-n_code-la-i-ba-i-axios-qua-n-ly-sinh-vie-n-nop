
const BASE_URL= "https://62db6dffe56f6d82a7729563.mockapi.io"

function getDSSV(){
    batLoading();
    axios({
        url:`${BASE_URL}/sv`,
        method: "GET",
    }).then(function(res){console.log(res);
        tatLoading();
        renderDSSV (res.data);
    })
    .catch(function(err){console.log(err);});
}
getDSSV();

function xoaSinhVien(id){
    axios({
        url:`${BASE_URL}/sv/${id}`,
        method: "DELETE",
    }).then(function(res){console.log(res);
        getDSSV();
    })
    .catch(function(err){console.log(err);});
};

function suaSinhVien(id){
    axios({
        url:`${BASE_URL}/sv/${id}`,
        method: "GET",
    }).then(function(res){console.log(res);
        showThongTinLenForm(res.data);
    })
    .catch(function(err){console.log(err);});
};



 function capNhat(){
    var sv = layThongTinTuForm();
    axios({
        url:`${BASE_URL}/sv/${sv.id}`,
        method: "PUT",
        data: sv,
    }).then(function(res){
        console.log(res);
        getDSSV();
    })
    .catch(function(err){console.log(err);});
 }


function themSV(){
    var sv = layThongTinTuForm();
    batLoading();
    axios({
        url:`${BASE_URL}/sv`,
        method: "POST",
        data: sv,
    }).then(function(res){
        tatLoading();
        console.log(res);
        getDSSV();
    })
    .catch(function(err){console.log(err);});
}
